
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Deck {
	private List<Card> cards = new ArrayList<>();

	public Deck() {
		for (int i = 0; i < Suit.values().length; i++) {
			for (int j = 1; j < 14; j++) {
				getCards().add(new Card(j, Suit.values()[i]));
			}
		}
	}
	
	public Card draw() {
		Card currentCard = getCards().get(0);
		getCards().remove(currentCard);
		return currentCard;
	}
	
	public void shuffle() {
		List<Card> shuffledCards = new ArrayList<>();
		Random random = new Random();
		Card card;

		while (getCards().size() > 0) {
			card = getCards().get(random.nextInt(getCards().size()));
			shuffledCards.add(card);
			getCards().remove(card);
		}
		
		setCards(shuffledCards);
	}

	public static boolean testDeckSizeBeforeAndAfterShuffle(Deck deck) {		
		int sizeBeforeShuffle = deck.getCards().size();

		deck.shuffle();
		
		return deck.getCards().size() == sizeBeforeShuffle;
	}
	
	public static boolean testDeckSizeBeforeAndAfterShuffle() {
		Deck deck = new Deck();
		
		return testDeckSizeBeforeAndAfterShuffle(deck);
	}

	public static boolean testDrawAndDeckSizeBeforeAndAfterShuffle() {
		Deck deck = new Deck();
		
		deck.draw();
		
		return testDeckSizeBeforeAndAfterShuffle(deck);
	}

	public static boolean testNoDuplicateCards(Deck deck) {
		while (deck.getCards().size() > 0) {
			Card card = deck.draw();
			
			for (int i = 0; i < deck.getCards().size(); i++) {
				if (card.equals(deck.getCards().get(i))) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	public static boolean testNoDuplicateCardsWithoutShuffle() {
		Deck deck = new Deck();
		
		return testNoDuplicateCards(deck);
	}

	public static boolean testNoDuplicateCardsWithShuffle() {
		Deck deck = new Deck();
		
		deck.shuffle();
		
		return testNoDuplicateCards(deck);
	}

	public static void main(String[] args) {
		System.out.println(testDeckSizeBeforeAndAfterShuffle());
		System.out.println(testDrawAndDeckSizeBeforeAndAfterShuffle());
		System.out.println(testNoDuplicateCardsWithoutShuffle());
		System.out.println(testNoDuplicateCardsWithShuffle());
	}

	public List<Card> getCards() {
		return cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}
}
