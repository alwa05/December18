import java.util.ArrayList;
import java.util.List;

public class Blackjack {
	private static Blackjack instance = new Blackjack();
	
	List<Card> hand = new ArrayList<>();
	
	public static Blackjack getInstance() {
		return instance;
	}
	
	public void hit() {
	}

	public void stand() {
	}

	public void reset() {
	}
	
	private int score() {
		int score = 0, value;

		for (int i = 0; i < getHand().size(); i++) {
			value = getHand().get(i).getValue();
			switch (value) {
			case 1:
				score += 11;
				break;
			case 11: case 12: case 13:
				score += 10;
				break;
			default:
				score += value;
				break;
			}
		}

		return score;
	}
	
	public int getScore() {
		int score = 0, value;

		for (int i = 0; i < getHand().size(); i++) {
			value = getHand().get(i).getValue();
			switch (value) {
			case 1:
				score += 11;
				break;
			case 11: case 12: case 13:
				score += 10;
				break;
			default:
				score += value;
				break;
			}
		}

		return score;
	}
	
	
	public static boolean testScore0() {
		Blackjack blackjack = new Blackjack();

		blackjack.getHand().add(new Card(1, Suit.HEARTS));
		blackjack.getHand().add(new Card(2, Suit.SPADES));

		return blackjack.score() == 13;
	}
	
	public static boolean testScore1() {
		Blackjack blackjack = new Blackjack();

		blackjack.getHand().add(new Card(9, Suit.HEARTS));
		blackjack.getHand().add(new Card(10, Suit.HEARTS));

		return blackjack.score() == 19;
	}
	
	public static boolean testScore2() {
		Blackjack blackjack = new Blackjack();

		blackjack.getHand().add(new Card(10, Suit.HEARTS));
		blackjack.getHand().add(new Card(11, Suit.HEARTS));

		return blackjack.score() == 20;
	}
	
	public static boolean testScore3() {
		Blackjack blackjack = new Blackjack();

		blackjack.getHand().add(new Card(12, Suit.HEARTS));
		blackjack.getHand().add(new Card(13, Suit.HEARTS));

		return blackjack.score() == 20;
	}

	public static void main(String[] args) {
		System.out.println(testScore0());
		System.out.println(testScore1());
		System.out.println(testScore2());
		System.out.println(testScore3());
	}

	public List<Card> getHand() {
		return hand;
	}

	public void setHand(List<Card> hand) {
		this.hand = hand;
	}
}