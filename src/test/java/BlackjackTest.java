
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class BlackjackTest {

	@BeforeEach
	void renewDeck() {
		Deck deck = new Deck();
		deck.shuffle();
	}

	@Test
	void testShuffle1() {
		Deck deck = new Deck();
		int deckSize = deck.getCards().size();
		deck.shuffle();
		int deckSizeAfterShuffle = deck.getCards().size();
		assertEquals(deckSizeAfterShuffle, deckSize, "Shuffled deck is 52");
	}

	@RepeatedTest(52)
	void testShuffle2() {
		Deck deck = new Deck();
		int deckSize = deck.getCards().size();
		deck.shuffle();
		deck.draw();
		assertEquals(deckSize, 52, "Shuffled deck is 52");
	}

	@Test
	void testShuffle3() {
		Deck deck = new Deck();
		int deckSize = deck.getCards().size();
		deck.draw();
		deck.shuffle();
		assertEquals(deckSize, 52, "Shuffled deck is 52");
	}

	@Test
	void testNewDeck1() {
		Deck deck = new Deck();
		while (!deck.getCards().isEmpty()) {
			Card card = deck.draw();
			if (deck.getCards().contains(card)) {
				assertTrue(deck.getCards().size() == 52);
			}
		}
	}

	void testNewDeck2() {
		Deck deck = new Deck();
		deck.shuffle();
		while (!deck.getCards().isEmpty()) {
			Card card = deck.draw();
			if (deck.getCards().contains(card)) {
				assertTrue(deck.getCards().size() == 52);
			}
		}
	}

	@ParameterizedTest
	@ValueSource(ints = { 0, 1, 2, 3 })
	void testScore1(int i) {
		Suit[] suits = Suit.values();
		assertNotNull(suits[i]);
	}

	@ParameterizedTest
	@ValueSource(ints = { 0, 1, 2, 3 })
	void testScore2(int i) {
		Suit[] suits = Suit.values();
		Blackjack blackjack = new Blackjack();
		blackjack.getHand().add(new Card(12, suits[i]));
		Card card0 = new Card(12, suits[i]);
		assertEquals(card0.getSuit(), (new Card(10, suits[i]).getSuit()));
	}

	@Test
	void testScore3() {
		Blackjack blackjack = new Blackjack();
		blackjack.getHand().add(new Card(13, Suit.CLUBS));
		blackjack.getHand().add(new Card(11, Suit.DIAMONDS));
		assertEquals(blackjack.getScore(), 20, "10 + 10 = 20");
	}

	@Test
	void testScore4() {
		Blackjack blackjack = new Blackjack();
		blackjack.getHand().add(new Card(13, Suit.CLUBS));
		blackjack.getHand().add(new Card(1, Suit.DIAMONDS));
		assertEquals(blackjack.getScore(), 21, "10 + 11= 21");
	}

	@Test
	void testScore5() {
		Blackjack blackjack = new Blackjack();
		blackjack.getHand().add(new Card(12, Suit.HEARTS));
		blackjack.getHand().add(new Card(10, Suit.SPADES));
		assertEquals(blackjack.getScore(), 20, "10 + 10= 20");
	}

	@ParameterizedTest
	@ValueSource(ints = { 2, 5, 10 })
	void testAce4(int i) {
		Blackjack blackjack = new Blackjack();
		blackjack.getHand().add(new Card(1, Suit.CLUBS));
		blackjack.getHand().add(new Card(1, Suit.HEARTS));
		blackjack.getHand().add(new Card(1, Suit.DIAMONDS));
		blackjack.getHand().add(new Card(1, Suit.SPADES));
		blackjack.getHand().add(new Card(7, Suit.HEARTS));
		i += blackjack.getScore();
		assertEquals(i + 11, +i, blackjack.getScore(), "11 + i = 11 + i");

	}
}
